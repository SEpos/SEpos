<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<select class="form-control">
										<option>Today</option>
										<option>Yesterday</option>
										<option>This Week</option>
										<option>This Month</option>
										<option>This Year</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Staff Name</th>
													<th>Start Time</th>
													<th>End Time</th>
													<th>Total Expected</th>
													<th>Total Actual</th>
													<th>Difference</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 50.700.000</td>
													<td class="text-success">+ Rp. 700.000</td>
												</tr>
												<tr>
													<td>Hanio</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 49.300.000</td>
													<td class="text-danger">- Rp. 700.000</td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 50.000.000</td>
													<td class="text-primary">Rp. 0</td>
												</tr>
												<tr>
													<td>Hanio</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 49.300.000</td>
													<td class="text-danger">- Rp. 700.000</td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 50.700.000</td>
													<td class="text-success">+ Rp. 700.000</td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>13.00</td>
													<td>20.00</td>
													<td>Rp. 50.000.000</td>
													<td>Rp. 50.700.000</td>
													<td class="text-success">+ Rp. 700.000</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>