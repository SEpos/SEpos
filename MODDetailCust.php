<div id="detail-cust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Customer Detail</h4>
			</div>
			<div class="modal-body">
				<div>
					<h3 class="mt0">Sebastian Wirajaya</h3>
				</div>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<th>Address</th>
								<td>Dalung Permai</td>
							</tr>
							<tr>
								<th>Phone</th>
								<td>081311020950</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>sebastianwirajaya@mail.com</td>
							</tr>
							<tr>
								<th>Birthday</th>
								<td>11-07-2000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary">Done</button>
			</div>
		</div>
	</div>
</div>
