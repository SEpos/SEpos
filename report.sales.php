<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<select class="form-control">
										<option>Today</option>
										<option>Yesterday</option>
										<option>This Week</option>
										<option>This Month</option>
										<option>This Year</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<tbody>
												<tr>
													<td style="border: none;">Gross Sales</td>
													<td style="border: none;" class="text-right">Rp. 50.000.000</td>
												</tr>
												<tr>
													<td>Discount</td>
													<td class="text-right">Rp. 10.000</td>
												</tr>
												<tr>
													<td>Refund</td>
													<td class="text-right">Rp. 10.000</td>
												</tr>
												<tr>
													<th style="border-top: 2px solid #ddd !important;">Net Sales</th>
													<th style="border-top: 2px solid #ddd !important;" class="text-right">Rp. 50.000</th>
												</tr>
												<tr>
													<td>Gratuity</td>
													<td class="text-right">Rp. 0</td>
												</tr>
												<tr>
													<td>Tax</td>
													<td class="text-right">Rp. 0</td>
												</tr>
												<tr>
													<th style="border-top: 2px solid #ddd !important;">Total Collected</th>
													<th style="border-top: 2px solid #ddd !important;" class="text-right">Rp. 50.000</th>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>