<div id="create-outlet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Create Outlet</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-12">
						<input type="text" placeholder="Outlet Name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<textarea placeholder="Address" class="form-control" rows="3"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<input type="number" placeholder="Phone" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-4">
						<input type="text" placeholder="City" class="form-control">
					</div>
					<div class="col-lg-4">
						<input type="text" placeholder="Province" class="form-control">
					</div>
					<div class="col-lg-4">
						<input type="text" placeholder="Postal Code" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>