<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search">
									<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-search"></i></button></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
							<div class="pull-right">
								<button type="button" class="btn btn-default mr">Outlet: 10</button>
								<button type="button" data-toggle="modal" data-target="#create-outlet" class="btn btn-primary">Create Outlet</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Outlet Name</th>
													<th>Address</th>
													<th>Phone</th>
													<th>City</th>
													<th>Province</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Outlet 1</td>
													<td>Jalan kemana saja I, no 9</td>
													<td>081234567890</td>
													<td>Denpasar</td>
													<td>Bali</td>
													<td><label class="label label-success">Active</label></td>
													<td>
														<button onclick="editTheOutlet('Outlet 1','Jalan kemana saja I, no 9','081234567890','Denpasar','Bali')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-outlet" title="Edit"><i class="fa fa-edit"></i></button>
														<button onclick="editTheOutlet('Outlet 1')" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-outlet" title="Close"><i class="fa fa-times"></i></button>
													</td>
												</tr>
												<tr>
													<td>Outlet 2</td>
													<td>Jalan kemana saja I, no 9</td>
													<td>081234567890</td>
													<td>Denpasar</td>
													<td>Bali</td>
													<td><label class="label label-warning">Pending</label></td>
													<td>
														<button onclick="editTheOutlet('Outlet 2','Jalan kemana saja I, no 9','081234567890','Denpasar','Bali')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-outlet" title="Edit" disabled><i class="fa fa-edit"></i></button>
														<button onclick="editTheOutlet('Outlet 2')" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-outlet" title="Close" disabled><i class="fa fa-times"></i></button>
													</td>
												</tr>
												<tr>
													<td>Outlet 5</td>
													<td>Jalan kemana saja I, no 9</td>
													<td>081234567890</td>
													<td>Denpasar</td>
													<td>Bali</td>
													<td><label class="label label-success">Active</label></td>
													<td>
														<button onclick="editTheOutlet('Outlet 5','Jalan kemana saja I, no 9','081234567890','Denpasar','Bali')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-outlet" title="Edit"><i class="fa fa-edit"></i></button>
														<button onclick="editTheOutlet('Outlet 5')" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-outlet" title="Close"><i class="fa fa-times"></i></button>
													</td>
												</tr>
												<tr>
													<td>Outlet 3</td>
													<td>Jalan kemana saja I, no 9</td>
													<td>081234567890</td>
													<td>Denpasar</td>
													<td>Bali</td>
													<td><label class="label label-danger">Close</label></td>
													<td>
														<button onclick="editTheOutlet('Outlet 3','Jalan kemana saja I, no 9','081234567890','Denpasar','Bali')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-outlet" title="Edit" disabled><i class="fa fa-edit"></i></button>
														<button onclick="editTheOutlet('Outlet 3')" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-outlet" title="Close" disabled><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
					<script src="assets/js/custom.js"></script>
<?php
include("footer.php");
?>