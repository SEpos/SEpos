<div id="create-disc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Create Discount</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-12">
						<input type="text" placeholder="Discount Name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-8">
						<input type="number" placeholder="Amount" class="form-control">
					</div>
					<div class="col-lg-4">
						<div data-toggle="buttons" class="btn-group">
							<label class="btn btn-primary active">
								<input type="radio" name="type" value="%" checked="1">%
							</label>
							<label class="btn btn-primary">
								<input type="radio" name="type" value="Rp" checked="">Rp
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>