<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<select class="form-control">
										<option>Today</option>
										<option>Yesterday</option>
										<option>This Week</option>
										<option>This Month</option>
										<option>This Year</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<h4 class="page-header mt text-uppercase">SALES SUMMARY</h4>
					<div class="row">
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-globe fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">Rp 100.000.000</div>
										<div class="text-uppercase">Gross Sales</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-wallet fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">Rp 100.000.000</div>
										<div class="text-uppercase">Net Sales</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-hourglass fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">Rp 100.000.000</div>
										<div class="text-uppercase">AVG. SALE PER TRANSACTION</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END widgets box-->
					<div class="row">
						<div class="col-lg-12">
							<div id="panelChart9" class="panel panel-default">
								<div class="panel-heading">
									<div class="panel-title">DAILY GROSS SALES AMOUNT</div>
								</div>
								<div class="panel-body">
									<div class="chart-spline flot-chart"></div>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>