<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="mt0 mb0 text-uppercase">Current Plan</h3>
								</div>
								<div class="panel-body">
Your current subscription plan: Trial<br />
Last Payment: N/A<br />
								</div>
								<div class="panel-heading">
									<h3 class="mt0 mb0 text-uppercase">Active Outlets</h3>
								</div>
								<div class="panel-body">
<b>Outlet 1</b><br />
Expiration Date : 24 June 2017<br />
<b>Outlet 3</b><br />
Expiration Date : 24 June 2017
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="mt0 mb0 text-uppercase">Billing</h3>
								</div>
								<div class="panel-body">
									<form class="form-horizontal">
										<div class="form-group">
											<label class="col-lg-3 control-label">Billing Cycle</label>
											<div class="col-lg-9">
												<select class="form-control">
													<option>Monthly</option>
													<option>6 Months</option>
													<option>12 Months</option>
												</select>
											</div>
										</div>
										<hr />
										<div class="table-responsive">
											<table class="table">
												<tbody>
													<tr>
														<th style="border: none;" colspan="2" class="text-uppercase text-primary">Outlets: 2</th>
													</tr>
													<tr>
														<th>Outlet 1</th>
														<td class="text-right">Rp 300.000/Month</td>
													</tr>
													<tr>
														<th>Outlet 2</th>
														<td class="text-right">Rp 300.000/Month</td>
													</tr>
													<tr>
														<th class="text-uppercase text-primary">12 Months</th>
														<td class="text-right text-primary">Rp 3.600.000</td>
													</tr>
												</tbody>
											</table>
										</div>
										<button class="btn btn-primary btn-block mt">Pay</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="mt0 mb0 text-uppercase">Billing History</h3>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Date</th>
													<th>Invoice</th>
													<th>Desc</th>
													<th>Amount</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>10-10-2017</td>
													<td>#123123</td>
													<td>Pay 2 outlets for a year.</td>
													<td>Rp 3.600.000</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>