<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<select class="form-control">
										<option>Today</option>
										<option>Yesterday</option>
										<option>This Week</option>
										<option>This Month</option>
										<option>This Year</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-globe fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">10</div>
										<div class="text-uppercase">TRANSACTIONS</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-basket-loaded fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">Rp 100.000.000</div>
										<div class="text-uppercase">TOTAL COLLECTED</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<!-- START widget-->
							<div class="panel widget bg-primary">
								<div class="row row-table">
									<div class="col-xs-4 text-center bg-primary-dark pv-lg">
										<em class="icon-wallet fa-3x"></em>
									</div>
									<div class="col-xs-8 pv-lg">
										<div class="h3 mt0">Rp 10.000.000</div>
										<div class="text-uppercase">NET SALES</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END widgets box-->
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<tbody>
												<tr class="trhead">
													<th colspan="5">Thursday, June 15, 2017</th>
													<th class="text-right">Rp 10.000.000</th>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 5.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 5.0000.0000</td>
												</tr>
											</tbody>
											<tbody>
												<tr class="trhead">
													<th colspan="5">Thursday, June 08, 2017</th>
													<th class="text-right">Rp 50.000.000</th>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 15.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 15.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 9</td>
													<td class="text-right">Rp 20.0000.0000</td>
												</tr>
											</tbody>
											<tbody>
												<tr class="trhead">
													<th colspan="5">Thursday, June 15, 2017</th>
													<th class="text-right">Rp 10.000.000</th>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 5.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 5.0000.0000</td>
												</tr>
											</tbody>
											<tbody>
												<tr class="trhead">
													<th colspan="5">Thursday, June 08, 2017</th>
													<th class="text-right">Rp 50.000.000</th>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 15.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 7</td>
													<td class="text-right">Rp 15.0000.0000</td>
												</tr>
												<tr>
													<td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#trans-detail"><i class="fa fa-search"></i></button></td>
													<td>Outlet 1</td>
													<td>10:38</td>
													<td>Sebastian Wirajaya</td>
													<td>iPhone 9</td>
													<td class="text-right">Rp 20.0000.0000</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>