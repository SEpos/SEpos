<div id="invite-staff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Invite Staff</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-6">
						<input type="text" placeholder="First Name" class="form-control">
					</div>
					<div class="col-lg-6">
						<input type="text" placeholder="Last Name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-6">
						<input type="email" placeholder="Email" class="form-control">
					</div>
					<div class="col-lg-6">
						<input type="text" placeholder="Title" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-6">
						<select class="form-control">
							<option>App Only</option>
							<option>App & Office</option>
						</select>
					</div>
					<div class="col-lg-6">
						<input type="text" placeholder="Invite Code" class="form-control" value="CW1107" readonly>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Send Invitation</button>
			</div>
			</form>
		</div>
	</div>
</div>