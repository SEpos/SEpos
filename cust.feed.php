<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<select class="form-control">
										<option>Today</option>
										<option>Yesterday</option>
										<option>This Week</option>
										<option>This Month</option>
										<option>This Year</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Date</th>
													<th>Name</th>
													<th>Comment</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
												<tr>
													<td>08-06-2017</td>
													<td>Sebastian Wirajaya</td>
													<td>Barangnya bagus gan, makasi gan</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>