<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<form>
								<div class="panel-body form-horizontal">
									<div class="form-group">
										<label class="col-lg-12 mb text-uppercase">Public Profile</label>
										<div class="col-lg-12">
											<input type="text" placeholder="Bussiness Name" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<textarea placeholder="Description" class="form-control" rows="3"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<textarea placeholder="Address" class="form-control" rows="3"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-4">
											<input type="text" placeholder="City" class="form-control">
										</div>
										<div class="col-lg-4">
											<input type="text" placeholder="State" class="form-control">
										</div>
										<div class="col-lg-4">
											<input type="text" placeholder="Zip" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-6">
											<input type="number" placeholder="Phone" class="form-control">
										</div>
										<div class="col-lg-6">
											<input type="email" placeholder="Email" class="form-control">
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<button class="btn btn-primary pull-right">Save</button>
									<div class="clearfix"></div>
								</div>
								</form>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>