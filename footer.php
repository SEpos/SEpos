				</div>
			</section>
			<!-- Page footer-->
			<footer>
				<span>&copy; 2017 - MAXPOS</span>
			</footer>
		</div>


<?php 
	include 'MODInviteStaff.php'; 
	include 'MODDeleteStaff.php';
	
	include 'MODCreateItem.php';
	include 'MODEditItem.php';
	include 'MODDeleteItem.php';
	
	include 'MODCreateCat.php';
	include 'MODEditCat.php';
	include 'MODDeleteCat.php';

	include 'MODCreateDisc.php';
	include 'MODCreateTax.php';

	include 'MODCreateOutlet.php';
	include 'MODEditOutlet.php';
	include 'MODDeleteOutlet.php';

?>

<div id="detail-cust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Customer Detail</h4>
			</div>
			<div class="modal-body">
				<div>
					<h3 class="mt0">Sebastian Wirajaya</h3>
				</div>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<th>Address</th>
								<td>Dalung Permai</td>
							</tr>
							<tr>
								<th>Phone</th>
								<td>081311020950</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>sebastianwirajaya@mail.com</td>
							</tr>
							<tr>
								<th>Birthday</th>
								<td>11-07-2000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary">Done</button>
			</div>
		</div>
	</div>
</div>

<div id="trans-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Transaction Detail</h4>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<h3>Apple Shop</h3>
					<p>Jalan kemana saja no 9, Denpasar, Bali, 80361</p>
					<p><i class="fa fa-phone"></i> 081311020950</p>
				</div>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td colspan="2">
15 Jun 2017<br />
Receipt Number<br />
Collected By
								</td>
								<td class="text-right">
10:38<br />
3BO4LF<br />
Sebastian Wirajaya
								</td>
							</tr>
							<tr>
								<td>iPhone 7</td>
								<td>x1</td>
								<td class="text-right">Rp. 10.000.000</td>
							</tr>
							<tr>
								<td colspan="2">Subtotal</td>
								<td class="text-right">Rp. 10.000.000</td>
							</tr>
							<tr>
								<td colspan="2">
<b>Total</b><br />
Cash<br />
Change
								</td>
								<td class="text-right">
<b>Rp. 10.000.000</b><br />
Rp. 10.000.000<br />
Rp. 0
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary">Done</button>
			</div>
		</div>
	</div>
</div>


		<!-- =============== VENDOR SCRIPTS ===============-->
		<!-- MODERNIZR-->
		<script src="assets/vendor/modernizr/modernizr.custom.js"></script>
		<!-- MATCHMEDIA POLYFILL-->
		<script src="assets/vendor/matchMedia/matchMedia.js"></script>
		<!-- JQUERY-->
		<script src="assets/vendor/jquery/dist/jquery.js"></script>
		<!-- BOOTSTRAP-->
		<script src="assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
		<!-- STORAGE API-->
		<script src="assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
		<!-- JQUERY EASING-->
		<script src="assets/vendor/jquery.easing/js/jquery.easing.js"></script>
		<!-- ANIMO-->
		<script src="assets/vendor/animo.js/animo.js"></script>
		<!-- SLIMSCROLL-->
		<script src="assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- SCREENFULL-->
		<script src="assets/vendor/screenfull/dist/screenfull.js"></script>
		<!-- LOCALIZE-->
		<script src="assets/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>
		<!-- RTL demo-->
		<script src="assets/js/demo/demo-rtl.js"></script>
		<!-- =============== PAGE VENDOR SCRIPTS ===============-->
		<!-- SPARKLINE-->
		<script src="assets/vendor/sparkline/index.js"></script>
		<!-- FLOT CHART-->
		<script src="assets/vendor/Flot/jquery.flot.js"></script>
		<script src="assets/vendor/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
		<script src="assets/vendor/Flot/jquery.flot.resize.js"></script>
		<script src="assets/vendor/Flot/jquery.flot.pie.js"></script>
		<script src="assets/vendor/Flot/jquery.flot.time.js"></script>
		<script src="assets/vendor/Flot/jquery.flot.categories.js"></script>
		<script src="assets/vendor/flot-spline/js/jquery.flot.spline.min.js"></script>
		<!-- CLASSY LOADER-->
		<script src="assets/vendor/jquery-classyloader/js/jquery.classyloader.min.js"></script>
		<!-- MOMENT JS-->
		<script src="assets/vendor/moment/min/moment-with-locales.min.js"></script>
		<!-- DEMO-->
		<script src="assets/js/demo/demo-flot.js"></script>
		<!-- =============== APP SCRIPTS ===============-->
		<script src="assets/js/app.js"></script>
	</body>
</html>