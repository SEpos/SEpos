<div id="delete-cat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Delete this Category?</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-6">
						<label>Category</label>
						<input type="text" placeholder="Category" class="form-control" value="iPhone" readonly>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Delete</button>
			</div>
			</form>
		</div>
	</div>
</div>