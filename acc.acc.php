<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<form>
								<div class="panel-body form-horizontal">
									<div class="form-group">
										<label class="col-lg-12 mb text-uppercase">Basic Information</label>
										<div class="col-lg-6">
											<input type="text" placeholder="First Name" class="form-control">
										</div>
										<div class="col-lg-6">
											<input type="text" placeholder="Last Name" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-12">
											<input type="email" placeholder="Email" value="sebastianwirajaya@mail.com" class="form-control" readonly>
										</div>
									</div>
									<hr />
									<div class="form-group">
										<label class="col-lg-12 mb text-uppercase">Change Password</label>
										<div class="col-lg-12">
											<input type="password" placeholder="Current Password" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-lg-6">
											<input type="password" placeholder="New Password" class="form-control">
										</div>
										<div class="col-lg-6">
											<input type="password" placeholder="Confirm New Password" class="form-control">
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<button class="btn btn-primary pull-right">Save</button>
									<div class="clearfix"></div>
								</div>
								</form>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>