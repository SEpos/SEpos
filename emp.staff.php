<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="pull-right">
								<button type="button" class="btn btn-default mr">Total: 10</button>
								<button type="button" data-toggle="modal" data-target="#invite-staff" class="btn btn-primary">Invite Staff</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Name</th>
													<th>Email</th>
													<th>Title</th>
													<th>Permissions</th>
													<th>Outlet</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>SebastianW</td>
													<td>sebastianwirajaya@mail.com</td>
													<td>BigBoss</td>
													<td>App & Office</td>
													<td>Outlet 1</td>
													<td>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-staff" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>SebastianW</td>
													<td>sebastianwirajaya@mail.com</td>
													<td>BigBoss</td>
													<td>App & Office</td>
													<td>Outlet 1</td>
													<td>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-staff" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>SebastianW</td>
													<td>sebastianwirajaya@mail.com</td>
													<td>BigBoss</td>
													<td>App & Office</td>
													<td>Outlet 1</td>
													<td>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-staff" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>SebastianW</td>
													<td>sebastianwirajaya@mail.com</td>
													<td>BigBoss</td>
													<td>App & Office</td>
													<td>Outlet 1</td>
													<td>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-staff" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>SebastianW</td>
													<td>sebastianwirajaya@mail.com</td>
													<td>BigBoss</td>
													<td>App & Office</td>
													<td>Outlet 1</td>
													<td>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-staff" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>