<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="MAXPOS">
		<meta name="keywords" content="MAXPOS">
		<title>MAXPOS</title>
		<link rel="icon" href="assets/img/logo-blue.png">
		<!-- =============== VENDOR STYLES ===============-->
		<!-- FONT AWESOME-->
		<link rel="stylesheet" href="assets/vendor/fontawesome/css/font-awesome.min.css">
		<!-- SIMPLE LINE ICONS-->
		<link rel="stylesheet" href="assets/vendor/simple-line-icons/css/simple-line-icons.css">
		<!-- =============== BOOTSTRAP STYLES ===============-->
		<link rel="stylesheet" href="assets/css/bootstrap.css" id="bscss">
		<!-- =============== APP STYLES ===============-->
		<link rel="stylesheet" href="assets/css/app.css" id="maincss">
	</head>
	<body>
		<div class="wrapper">
			<div class="block-center mt-xl wd-xl">
				<!-- START panel-->
				<div class="panel panel-dark panel-flat">
					<div class="panel-heading text-center" style="padding: 20px 0;">
						<a href="login.php" style="color: #fff; font-weight: bold;">
							<img src="assets/img/logo-full.png" width="100px">
						</a>
					</div>
					<div class="panel-body">
						<p class="text-center pv">Register to Maxpos.</p>
						<div role="alert" class="alert alert-danger alert-dismissible fade in">
							<button type="button" data-dismiss="alert" aria-label="Close" class="close">
								<span aria-hidden="true">&times;</span>
							</button>
							<strong>Ops!</strong> Please enter a valid email.
						</div>
						<form role="form" data-parsley-validate="" novalidate="" class="mb-lg">
							<div class="form-group has-feedback">
								<input id="exampleInputEmail1" type="email" placeholder="Email" autocomplete="off" required class="form-control">
								<span class="fa fa-envelope form-control-feedback text-muted"></span>
							</div>
							<div class="form-group has-feedback">
								<input id="exampleInputPassword1" type="password" placeholder="Password" required class="form-control">
								<span class="fa fa-lock form-control-feedback text-muted"></span>
							</div>
							<div class="form-group has-feedback">
								<input id="exampleInputPassword1" type="password" placeholder="Confirm Password" required class="form-control">
								<span class="fa fa-lock form-control-feedback text-muted"></span>
							</div>
							<div class="clearfix">
								<div class="checkbox c-checkbox pull-left mt0">
									<label>
									<input type="checkbox" value="1" name="terms">
									<span class="fa fa-check"></span>Accept terms of service</label>
								</div>
							</div>
							<!--<button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>-->
							<a href="index.php" class="btn btn-block btn-primary mt-lg">Register</a>
						</form>
						<p class="pt-lg text-center">Have an account?</p>
						<a href="login.php" class="btn btn-block btn-primary">Login Now</a>
					</div>
				</div>
				<!-- END panel-->
				<div class="p-lg text-center">
					<span>&copy;</span>
					<span><?php echo date("Y"); ?></span>
					<span>-</span>
					<span>Maxpos</span>
				</div>
			</div>
		</div>
		<!-- =============== VENDOR SCRIPTS ===============-->
		<!-- MODERNIZR-->
		<script src="assets/vendor/modernizr/modernizr.custom.js"></script>
		<!-- JQUERY-->
		<script src="assets/vendor/jquery/dist/jquery.js"></script>
		<!-- BOOTSTRAP-->
		<script src="assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
		<!-- STORAGE API-->
		<script src="assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
		<!-- PARSLEY-->
		<script src="assets/vendor/parsleyjs/dist/parsley.min.js"></script>
		<!-- =============== APP SCRIPTS ===============-->
		<script src="assets/js/app.js"></script>
	</body>
</html>