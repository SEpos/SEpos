<div id="delete-outlet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Close this Outlet?</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-12">
						<label>Outlet Name</label>
						<input id="CloseName" type="text" placeholder="Outlet Name" class="form-control" value="Outlet 1" readonly>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<label>Reason</label>
						<textarea placeholder="Reason" class="form-control" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Send Request</button>
			</div>
			</form>
		</div>
	</div>
</div>