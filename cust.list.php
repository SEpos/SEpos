<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search name, email, or phone">
									<span class="input-group-btn">
										<button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="pull-right">
								<button type="button" data-toggle="modal" data-target="#create-item" class="btn btn-primary">Export Customer</button>
								<button type="button" data-toggle="modal" data-target="#create-item" class="btn btn-primary">Import Customer</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Name</th>
													<th>Since</th>
													<th>This Month</th>
													<th>This Year</th>
													<th>Lifetime</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>08-06-2017</td>
													<td>Rp 5.000.000</td>
													<td>Rp 5.000.000</td>
													<td>Rp 109.000.000</td>
													<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-cust"><i class="fa fa-search"></i></button></td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>08-06-2017</td>
													<td>Rp 5.000.000</td>
													<td>Rp 5.000.000</td>
													<td>Rp 109.000.000</td>
													<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-cust"><i class="fa fa-search"></i></button></td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>08-06-2017</td>
													<td>Rp 5.000.000</td>
													<td>Rp 5.000.000</td>
													<td>Rp 109.000.000</td>
													<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-cust"><i class="fa fa-search"></i></button></td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>08-06-2017</td>
													<td>Rp 5.000.000</td>
													<td>Rp 5.000.000</td>
													<td>Rp 109.000.000</td>
													<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-cust"><i class="fa fa-search"></i></button></td>
												</tr>
												<tr>
													<td>Sebastian Wirajaya</td>
													<td>08-06-2017</td>
													<td>Rp 5.000.000</td>
													<td>Rp 5.000.000</td>
													<td>Rp 109.000.000</td>
													<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-cust"><i class="fa fa-search"></i></button></td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>