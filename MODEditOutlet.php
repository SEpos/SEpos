<div id="edit-outlet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Edit Outlet</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-12">
						<input type="text" placeholder="Outlet Name" id="Outlet_Name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<textarea placeholder="Address" class="form-control" id="Address" rows="3"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<input type="number" placeholder="Phone" id="Phone" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-4">
						<input type="text" placeholder="City" id="City" class="form-control">
					</div>
					<div class="col-lg-4">
						<input type="text" placeholder="Province" id="Province" class="form-control">
					</div>
					<div class="col-lg-4">
						<input type="text" placeholder="Postal Code" id="Postal_Code" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>