<div id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Create Item</h4>
			</div>
			<form>
			<div class="modal-body form-horizontal">
				<div class="form-group">
					<div class="col-lg-12">
						<input type="text" placeholder="Item Name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<select class="form-control">
							<option>Uncategorized</option>
							<option>iPhone</option>
							<option>Android</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-6">
						<input type="number" placeholder="Price" class="form-control">
					</div>
					<div class="col-lg-6">
						<input type="text" placeholder="SKU" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<textarea placeholder="Item Description" class="form-control" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>