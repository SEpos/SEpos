<div id="trans-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-dismiss="modal" aria-label="Close" class="close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="myModalLabel" class="modal-title">Transaction Detail</h4>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<h3>Apple Shop</h3>
					<p>Jalan kemana saja no 9, Denpasar, Bali, 80361</p>
					<p><i class="fa fa-phone"></i> 081311020950</p>
				</div>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td colspan="2">
									15 Jun 2017<br />
									Receipt Number<br />
									Collected By
								</td>
								<td class="text-right">
									10:38<br />
									3BO4LF<br />
									Sebastian Wirajaya
								</td>
							</tr>
							<tr>
								<td>iPhone 7</td>
								<td>x1</td>
								<td class="text-right">Rp. 10.000.000</td>
							</tr>
							<tr>
								<td colspan="2">Subtotal</td>
								<td class="text-right">Rp. 10.000.000</td>
							</tr>
							<tr>
								<td colspan="2">
									<b>Total</b><br />
									Cash<br />
									Change
								</td>
								<td class="text-right">
									<b>Rp. 10.000.000</b><br />
									Rp. 10.000.000<br />
									Rp. 0
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary">Done</button>
			</div>
		</div>
	</div>
</div>