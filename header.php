<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="MAXPOS">
		<meta name="keywords" content="MAXPOS">
		<title>MAXPOS</title>
		<link rel="icon" href="assets/img/logo-blue.png">
		<!-- =============== VENDOR STYLES ===============-->
		<!-- FONT AWESOME-->
		<link rel="stylesheet" href="assets/vendor/fontawesome/css/font-awesome.min.css">
		<!-- SIMPLE LINE ICONS-->
		<link rel="stylesheet" href="assets/vendor/simple-line-icons/css/simple-line-icons.css">
		<!-- ANIMATE.CSS-->
		<link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
		<!-- WHIRL (spinners)-->
		<link rel="stylesheet" href="assets/vendor/whirl/dist/whirl.css">
		<!-- =============== PAGE VENDOR STYLES ===============-->
		<!-- WEATHER ICONS-->
		<link rel="stylesheet" href="assets/vendor/weather-icons/css/weather-icons.min.css">
		<!-- =============== BOOTSTRAP STYLES ===============-->
		<link rel="stylesheet" href="assets/css/bootstrap.css" id="bscss">
		<!-- =============== APP STYLES ===============-->
		<link rel="stylesheet" href="assets/css/app.css" id="maincss">

<style type="text/css">
.trhead {
	background: #EEEEEE;
}
.bnone {
	border: none;
}
</style>

	</head>
	<body>
		<div class="wrapper">
			<!-- top navbar-->
			<header class="topnavbar-wrapper">
				<!-- START Top Navbar-->
				<nav role="navigation" class="navbar topnavbar">
					<!-- START navbar header-->
					<div class="navbar-header">
						<a href="index.php" class="navbar-brand">
							<div class="brand-logo">
								<img src="assets/img/poslogo.png" class="img-responsive" width="60px">
							</div>
							<div class="brand-logo-collapsed">
								<img src="assets/img/poslogo.png" class="img-responsive" width="40px" style="padding: 5px;">
							</div>
							<!--<div class="brand-logo" style="color: #fff; padding: 15px;">
								MAXPOS
							</div>
							<div class="brand-logo-collapsed" style="color: #fff; padding: 15px;">
								M
							</div>-->
						</a>
					</div>
					<!-- END navbar header-->
					<!-- START Nav wrapper-->
					<div class="nav-wrapper">
						<!-- START Left navbar-->
						<ul class="nav navbar-nav">
							<li>
								<!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
								<a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
								<em class="fa fa-navicon"></em>
								</a>
								<!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
								<a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
								<em class="fa fa-navicon"></em>
								</a>
							</li>
						</ul>
						<!-- END Left navbar-->
						<!-- START Right Navbar-->
						<ul class="nav navbar-nav navbar-right">
							<!-- START Alert menu-->
							<li class="dropdown dropdown-list">
								<a href="#" data-toggle="dropdown">
									<em class="icon-bell"></em>
									<div class="label label-danger">11</div>
								</a>
								<!-- START Dropdown menu-->
								<ul class="dropdown-menu animated flipInX">
									<li>
										<!-- START list group-->
										<div class="list-group">
											<!-- list item-->
											<a href="#" class="list-group-item">
												<div class="media-box">
													<div class="pull-left">
														<em class="fa fa-twitter fa-2x text-info"></em>
													</div>
													<div class="media-box-body clearfix">
														<p class="m0">New followers</p>
														<p class="m0 text-muted">
															<small>1 new follower</small>
														</p>
													</div>
												</div>
											</a>
											<!-- list item-->
											<a href="#" class="list-group-item">
												<div class="media-box">
													<div class="pull-left">
														<em class="fa fa-envelope fa-2x text-warning"></em>
													</div>
													<div class="media-box-body clearfix">
														<p class="m0">New e-mails</p>
														<p class="m0 text-muted">
															<small>You have 10 new emails</small>
														</p>
													</div>
												</div>
											</a>
											<!-- list item-->
											<a href="#" class="list-group-item">
												<div class="media-box">
													<div class="pull-left">
														<em class="fa fa-tasks fa-2x text-success"></em>
													</div>
													<div class="media-box-body clearfix">
														<p class="m0">Pending Tasks</p>
														<p class="m0 text-muted">
															<small>11 pending task</small>
														</p>
													</div>
												</div>
											</a>
											<!-- last list item-->
											<a href="#" class="list-group-item">
											<small>More notifications</small>
											<span class="label label-danger pull-right">14</span>
											</a>
										</div>
										<!-- END list group-->
									</li>
								</ul>
								<!-- END Dropdown menu-->
							</li>
							<!-- END Alert menu-->
							<!-- START Offsidebar button-->
							<li>
								<a href="login.php">
								<em class="icon-logout"></em>
								</a>
							</li>
							<!-- END Offsidebar menu-->
						</ul>
						<!-- END Right Navbar-->
					</div>
					<!-- END Nav wrapper-->
				</nav>
				<!-- END Top Navbar-->
			</header>
			<!-- sidebar-->
			<aside class="aside">
				<!-- START Sidebar (left)-->
				<div class="aside-inner">
					<nav data-sidebar-anyclick-close="" class="sidebar">
						<!-- START sidebar nav-->
						<ul class="nav">
							<!-- Iterates over all sidebar items-->
							<li class="nav-heading ">
								<span>Navigation</span>
							</li>

							<li>
								<a href="index.php">
									<em class="fa fa-home"></em>
									<span>Dashboard</span>
								</a>
							</li>

							<li>
								<a href="#reports" data-toggle="collapse">
									<em class="fa fa-file-text"></em>
									<span>Reports</span>
								</a>
								<ul id="reports" class="nav sidebar-subnav collapse">
									<li><a href="report.sales.php"><span>Sales</span></a></li>
									<li><a href="report.trans.php"><span>Transaction</span></a></li>
									<li><a href="report.shift.php"><span>Shift</span></a></li>
								</ul>
							</li>

							<li>
								<a href="#inv" data-toggle="collapse">
									<em class="fa fa-suitcase"></em>
									<span>Inventory</span>
								</a>
								<ul id="inv" class="nav sidebar-subnav collapse">
									<li><a href="inven.item.php"><span>Item List</span></a></li>
									<li><a href="inven.cat.php"><span>Categories</span></a></li>
									<li><a href="inven.tax.php"><span>Taxes</span></a></li>
									<li><a href="inven.disc.php"><span>Discount</span></a></li>
								</ul>
							</li>

							<li>
								<a href="#storage" data-toggle="collapse">
									<em class="fa fa-cubes"></em>
									<span>Storage</span>
								</a>
								<ul id="storage" class="nav sidebar-subnav collapse">
									<li><a href="storage.sum.php"><span>Sumary</span></a></li>
									<li><a href="storage.sup.php"><span>Suppliers</span></a></li>
									<li><a href="storage.po.php"><span>Purchase Order</span></a></li>
									<li><a href="storage.trans.php"><span>Transfer</span></a></li>
									<li><a href="storage.adj.php"><span>Adjustment</span></a></li>
								</ul>
							</li>

							<li>
								<a href="#cust" data-toggle="collapse">
									<em class="fa fa-users"></em>
									<span>Customers</span>
								</a>
								<ul id="cust" class="nav sidebar-subnav collapse">
									<li><a href="cust.list.php"><span>Customers List</span></a></li>
									<li><a href="cust.feed.php"><span>Feedback</span></a></li>
								</ul>
							</li>

							<li>
								<a href="#emp" data-toggle="collapse">
									<em class="fa fa-star"></em>
									<span>Employees</span>
								</a>
								<ul id="emp" class="nav sidebar-subnav collapse">
									<li><a href="emp.staff.php"><span>Staff</span></a></li>
									<li><a href="emp.perm.php"><span>Permissions</span></a></li>
								</ul>
							</li>

							<li>
								<a href="outlets.php">
									<em class="fa fa-building"></em>
									<span>Outlets</span>
								</a>
							</li>

							<li>
								<a href="#acc" data-toggle="collapse">
									<em class="fa fa-gear"></em>
									<span>Account Settings</span>
								</a>
								<ul id="acc" class="nav sidebar-subnav collapse">
									<li><a href="acc.acc.php"><span>Account</span></a></li>
									<li><a href="acc.billing.php"><span>Billing</span></a></li>
									<li><a href="acc.public.php"><span>Public Profile</span></a></li>
									<li><a href="acc.recpt.php"><span>Receipt</span></a></li>
									<li><a href="acc.check.php"><span>Checkout</span></a></li>
									<li><a href="acc.email.php"><span>Email Notification</span></a></li>
								</ul>
							</li>

						</ul>
						<!-- END sidebar nav-->
					</nav>
				</div>
				<!-- END Sidebar (left)-->
			</aside>
			<!-- offsidebar-->
			<section>
				<!-- Page content-->
				<div class="content-wrapper">