<?php
include("header.php");
?>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<select class="form-control">
										<option>All Outlets</option>
										<option>Outlet 1</option>
										<option>Outlet 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search">
									<span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-search"></i></button></span>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pull-right">
								<button type="button" data-toggle="modal" data-target="#create-cat" class="btn btn-primary mb">Create Category</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Category Name</th>
													<th>Item Stocks</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>iPhone</td>
													<td>10 items</td>
													<td>
														<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-cat" title="Edit"><i class="fa fa-edit"></i></button>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-cat" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>Android</td>
													<td>15 items</td>
													<td>
														<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-cat" title="Edit"><i class="fa fa-edit"></i></button>
														<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-cat" title="Delete"><i class="fa fa-trash"></i></button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<nav class="text-center mt">
										<ul class="pagination pagination-sm m0">
											<li>
												<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
											<li><a href="#">1</a>
											</li>
											<li><a href="#">2</a>
											</li>
											<li class="active"><a href="#">3</a>
											</li>
											<li><a href="#">4</a>
											</li>
											<li><a href="#">5</a>
											</li>
											<li>
												<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<?php
include("footer.php");
?>